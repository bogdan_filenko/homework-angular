using System.Threading.Tasks;
using System.Collections.Generic;
using Homework.Common.DTOs.Project;

namespace Homework.BLL.Interfaces
{
    public interface IProjectsService
    {
        Task<IEnumerable<ProjectDTO>> GetProjects();
        Task<ProjectDTO> GetProjectById(int id);
        Task<ProjectDTO> CreateProject(CreateProjectDTO createDto);
        Task<ProjectDTO> UpdateProject(UpdateProjectDTO updateDto);
        Task DeleteProject(int id);
    }
}