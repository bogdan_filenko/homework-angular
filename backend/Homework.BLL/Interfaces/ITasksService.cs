using System.Threading.Tasks;
using Homework.Common.DTOs.Task;
using System.Collections.Generic;

namespace Homework.BLL.Interfaces
{
    public interface ITasksService
    {
        Task<IEnumerable<TaskDTO>> GetTasks();
        Task<TaskDTO> GetTaskById(int id);
        Task<TaskDTO> CreateTask(CreateTaskDTO createDto);
        Task<TaskDTO> UpdateTask(UpdateTaskDTO updateDto);
        Task DeleteTask(int id);
    }
}