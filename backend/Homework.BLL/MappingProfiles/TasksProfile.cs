using System;
using AutoMapper;
using Homework.DAL.Entities;
using Homework.Common.DTOs.Task;

namespace Homework.BLL.MappingProfiles
{
    public sealed class TasksProfile : Profile
    {
        public TasksProfile()
        {
            CreateMap<Task, TaskDTO>();

            CreateMap<CreateTaskDTO, Task>()
                .ForMember(dest => dest.CreatedAt, src => src.MapFrom(t => DateTime.Now))
                .ForMember(dest => dest.State, src => src.MapFrom(t => 0));
        }
    }
}