using System;

namespace Homework.BLL.Exceptions
{
    public sealed class EntityExistsException : Exception
    {
        public EntityExistsException(Type type, string propertyName, string propertyValue)
            : base($"An entity with type {type} and {propertyName} {propertyValue} already exists")
        { }

    }
}