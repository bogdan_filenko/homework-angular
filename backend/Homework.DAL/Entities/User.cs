using System;
using System.Collections.Generic;

#nullable enable

namespace Homework.DAL.Entities
{
    public sealed class User
    {
        public int Id { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? Email { get; set; }
        public DateTime RegisteredAt { get; set; }
        public DateTime BirthDate { get; set; }
        public int? TeamId { get; set; }

        public Team? Team { get; private set; }
        public ICollection<Project> Projects { get; private set; }
        public ICollection<Task> Tasks { get; private set; }
    }
}