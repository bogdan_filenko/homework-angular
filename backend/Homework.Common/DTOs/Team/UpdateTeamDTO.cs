#nullable enable

namespace Homework.Common.DTOs.Team
{
    public sealed class UpdateTeamDTO
    {
        public int Id { get; set; }
        public string? Name { get; set; }
    }
}