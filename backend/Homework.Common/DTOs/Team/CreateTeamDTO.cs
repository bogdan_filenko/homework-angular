#nullable enable

namespace Homework.Common.DTOs.Team
{
    public sealed class CreateTeamDTO
    {
        public string? Name { get; set; }
    }
}