using System;

#nullable enable

namespace Homework.Common.DTOs.Project
{
    public sealed class UpdateProjectDTO
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }
        public DateTime Deadline { get; set; }
    }
}