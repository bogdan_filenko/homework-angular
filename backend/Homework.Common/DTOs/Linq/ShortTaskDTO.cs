#nullable enable

namespace Homework.Common.DTOs.Linq
{
    public sealed class ShortTaskDTO
    {
        public int Id { get; set; }
        public string? Name { get; set; }

        public override string ToString()
        {
            return  $"Id: {this.Id}\n" +
                    $"Name: {this.Name}";
        }
    }
}