using System.Text;

#nullable enable

namespace Homework.Common.DTOs.Linq
{
    public sealed class ShortTeamDTO
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public UserLinqDTO[]? Members { get; set; }

        public override string ToString()
        {
            StringBuilder viewBuilder = new StringBuilder();
            
            viewBuilder.Append( $"Id: {this.Id}\n" +
                                $"Name: {this.Name}\n" +
                                "Users:\n");
            foreach (var user in this.Members)
            {
                viewBuilder.Append(
                    $"\tId: {user.Id}\n" +
                    $"\tName: {user.FirstName + " " + user.LastName}\n" +
                    $"\tEmail: {user.Email}\n\n"
                );
            }
            
            return viewBuilder.ToString();
        }
    }
}