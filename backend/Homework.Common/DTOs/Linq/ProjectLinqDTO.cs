using System;

#nullable enable

namespace Homework.Common.DTOs.Linq
{
    public sealed class ProjectLinqDTO
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }
        public DateTime Deadline { get; set; }
        public DateTime CreatedAt { get; set; }

        public TaskLinqDTO[]? Tasks { get; set; }
        public UserLinqDTO Author { get; set; }
        public TeamLinqDTO? Team { get; set; }

        public override string ToString()
        {
            return "Project:\n" +
                    $"\tId: {this.Id}\n" +
                    $"\tName: {this.Name}\n" +
                    $"\tDescription: {this.Description}\n" +
                    $"\tDeadline: {this.Deadline.ToString()}\n" +
                    $"\tCreated at: {this.CreatedAt.ToString()}";
        }
    }
}