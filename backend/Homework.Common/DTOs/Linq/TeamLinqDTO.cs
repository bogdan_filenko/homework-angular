using System;
using System.Text;

#nullable enable

namespace Homework.Common.DTOs.Linq
{
    public sealed class TeamLinqDTO
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public DateTime CreatedAt { get; set; }

        public UserLinqDTO[]? Members { get; set; }

        public override string ToString()
        {
            StringBuilder viewBuilder = new StringBuilder();
            
            viewBuilder.Append( $"Id: {this.Id}\n" +
                                $"Name: {this.Name}\n" +
                                $"Created at: {this.CreatedAt.ToString()}");
            
            return viewBuilder.ToString();
        }
    }
}