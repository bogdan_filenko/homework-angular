using Homework.BLL.Services;
using Homework.BLL.Interfaces;
using Microsoft.OpenApi.Models;
using Homework.BLL.MappingProfiles;
using Microsoft.Extensions.DependencyInjection;

namespace Homework.WebAPI.Extensions
{
    public static class ServicesExtensions
    {
        public static void RegisterBusinessServices(this IServiceCollection services)
        {
            services.AddTransient<ILinqService, LinqService>();
            services.AddTransient<IProjectsService, ProjectsService>();
            services.AddTransient<ITasksService, TasksService>();
            services.AddTransient<IUsersService, UsersService>();
            services.AddTransient<ITeamsService, TeamsService>();
            services.AddTransient<IUnfinishedTasksService, UnfinishedTasksService>();
        }

        public static void RegisterAutomapper(this IServiceCollection services)
        {
            services.AddAutoMapper(cfg =>
            {
                cfg.AddProfile<ProjectsProfile>();
                cfg.AddProfile<TeamsProfile>();
                cfg.AddProfile<TasksProfile>();
                cfg.AddProfile<UsersProfile>();
                cfg.AddProfile<LinqProfile>();
            });
        }

        public static void RegisterSwagger(this IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Homework API", Version = "v1"});
            });
        }
    }
}