using System.Linq;
using System.Threading.Tasks;
using Homework.BLL.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Homework.Common.DTOs.Linq;
using System.Collections.Generic;

namespace Homework.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class LinqController : ControllerBase
    {
        private readonly ILinqService _linqService;
        
        public LinqController(ILinqService linqService)
        {
            _linqService = linqService;
        }

        [HttpGet("users/{id}/projects/tasks")]
        public async Task<ActionResult<KeyValuePair<ProjectLinqDTO, int>[]>> GetProjectsTasksNumber(int id)
        {
            var projectTasksNumberDictionary = await _linqService.GetProjectsTaskNumber(id);

            return Ok(projectTasksNumberDictionary.ToArray());
        }

        [HttpGet("users/{id}/tasks")]
        public async Task<ActionResult<IEnumerable<TaskLinqDTO>>> GetAllUserTasks(int id)
        {
            return Ok(await _linqService.GetAllUserTasks(id));
        }

        [HttpGet("users/{id}/tasks/finished")]
        public async Task<ActionResult<IEnumerable<ShortTaskDTO>>> GetUserFinishedTasks(int id)
        {
            return Ok(await _linqService.GetFinishedTasks(id));
        }

        [HttpGet("teams/users/ordered")]
        public async Task<ActionResult<IEnumerable<ShortTeamDTO>>> GetTeamsWhereUsersOlderThan10()
        {
            return Ok(await _linqService.GetTeamsWhereUsersOlderThan10());
        }

        [HttpGet("users/tasks/ordered")]
        public async Task<ActionResult<IEnumerable<UserLinqDTO>>> GetUsersWithTasksOrdered()
        {
            return Ok(await _linqService.GetUsersWithSortedTasks());
        }

        [HttpGet("users/{id}/info")]
        public async Task<ActionResult<UserInfoDTO>> GetUserInfo(int id)
        {
            return Ok(await _linqService.GetUserInfo(id));
        }

        [HttpGet("projects/info")]
        public async Task<ActionResult<IEnumerable<ProjectInfoDTO>>> GetProjectsInfo()
        {
            return Ok(await _linqService.GetProjectsInfo());
        }
    }
}