import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser/';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app.routes';
import { AppComponent } from './app.component';
import { ProjectsListComponent } from './components/projects-list/projects-list.component';
import { ProjectComponent } from './components/project/project.component';
import { TypeofPipe } from './pipes/typeof.pipe';
import { TaskComponent } from './components/task/task.component';
import { TeamComponent } from './components/team/team.component';
import { UserComponent } from './components/user/user.component';
import { HttpClientService } from './services/http-client.service';
import { HttpClientModule } from '@angular/common/http';
import { TasksListComponent } from './components/tasks-list/tasks-list.component';
import { TeamsListComponent } from './components/teams-list/teams-list.component';
import { UsersListComponent } from './components/users-list/users-list.component';
import { DateUAPipe } from './pipes/dateUA.pipe';
import { MaterialsModule } from './common/modules/materials.module';
import { UpdateProjectComponent } from './components/update-project/update-project.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UpdateTaskComponent } from './components/update-task/update-task.component';
import { UpdateTeamComponent } from './components/update-team/update-team.component';
import { UpdateUserComponent } from './components/update-user/update-user.component';
import { CreateProjectComponent } from './components/create-project/create-project.component';
import { CreateTaskComponent } from './components/create-task/create-task.component';
import { CreateTeamComponent } from './components/create-team/create-team.component';
import { CreateUserComponent } from './components/create-user/create-user.component';
import { FinishedDirective } from './directives/finished.directive';

@NgModule({
  declarations: [
    AppComponent,
    ProjectsListComponent,
    ProjectComponent,
    TaskComponent,
    TeamComponent,
    UserComponent,
    TasksListComponent,
    TeamsListComponent,
    UsersListComponent,
    UpdateProjectComponent,
    UpdateTaskComponent,
    UpdateTeamComponent,
    UpdateUserComponent,
    CreateProjectComponent,
    CreateTaskComponent,
    CreateTeamComponent,
    CreateUserComponent,
    FinishedDirective,
    TypeofPipe,
    DateUAPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MaterialsModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule
  ],
  providers: [
    HttpClientService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
