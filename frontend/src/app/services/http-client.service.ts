import { Injectable } from "@angular/core";
import { environment } from "environments/environment";
import { HttpClient } from '@angular/common/http';
import { Observable } from "rxjs";


@Injectable({ providedIn: 'root' })
export class HttpClientService {
    public routePrefix: string;

    constructor(private http: HttpClient) {
        this.routePrefix = environment.apiRoute;
    }

    public get<TReturn>(url: string): Observable<TReturn> {
        return this.http.get<TReturn>(this.buildUrl(url));
    }

    public post<TInput, VReturn>(url: string, entity: TInput): Observable<VReturn> {
        return this.http.post<VReturn>(this.buildUrl(url), entity);
    }

    public put<TInput, VReturn>(url: string, entity: TInput): Observable<VReturn> {
        return this.http.put<VReturn>(this.buildUrl(url), entity);
    }

    public delete(url: string): Observable<void> {
        return this.http.delete<void>(this.buildUrl(url));
    }

    private buildUrl(url: string): string {
        return this.routePrefix + url;
    }
}