import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProjectsListComponent } from './components/projects-list/projects-list.component';
import { UsersListComponent } from './components/users-list/users-list.component';
import { TeamsListComponent } from './components/teams-list/teams-list.component';
import { UpdateProjectComponent } from './components/update-project/update-project.component';
import { UpdateTaskComponent } from './components/update-task/update-task.component';
import { UpdateTeamComponent } from './components/update-team/update-team.component';
import { UpdateUserComponent } from './components/update-user/update-user.component';
import { TasksListComponent } from './components/tasks-list/tasks-list.component';
import { CreateProjectComponent } from './components/create-project/create-project.component';
import { CreateTaskComponent } from './components/create-task/create-task.component';
import { CreateTeamComponent } from './components/create-team/create-team.component';
import { CreateUserComponent } from './components/create-user/create-user.component';
import { CloseCRUDGuard } from './guards/close-crud.guard';

const routes: Routes = [
  { path: 'Projects', component: ProjectsListComponent },
  { path: 'Projects/:id/update', component: UpdateProjectComponent, canDeactivate: [CloseCRUDGuard] },
  { path: 'Projects/create', component: CreateProjectComponent, canDeactivate: [CloseCRUDGuard] },
  { path: 'Projects/:id/Tasks', component: TasksListComponent },
  { path: 'Projects/:id/Tasks/:id/update', component: UpdateTaskComponent, canDeactivate: [CloseCRUDGuard] },
  { path: 'Projects/:id/Tasks/create', component: CreateTaskComponent, canDeactivate: [CloseCRUDGuard] },
  { path: 'Users', component: UsersListComponent },
  { path: 'Users/:id/update', component: UpdateUserComponent, canDeactivate: [CloseCRUDGuard] },
  { path: 'Users/create', component: CreateUserComponent, canDeactivate: [CloseCRUDGuard] },
  { path: 'Teams', component: TeamsListComponent },
  { path: 'Teams/:id/update', component: UpdateTeamComponent, canDeactivate: [CloseCRUDGuard] },
  { path: 'Teams/create', component: CreateTeamComponent, canDeactivate: [CloseCRUDGuard] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
