import { Component, OnDestroy, OnInit } from "@angular/core";
import { FormGroup } from "@angular/forms";
import { ActivatedRoute, Params, Router } from "@angular/router";
import { Subject } from "rxjs";
import { map, takeUntil } from "rxjs/operators";
import { CRUDItemComponent } from "src/app/common/abstractions/crud-item-component";
import { updateUserGroup } from "src/app/common/form-groups/userGroups";
import { Team } from "src/app/models/team/team";
import { UpdateUser } from "src/app/models/user/update-user";
import { User } from "src/app/models/user/user";
import { HttpClientService } from "src/app/services/http-client.service";

@Component({
    selector: 'app-update-user',
    templateUrl: 'update-user.component.html',
    styleUrls: [ './../../common/styles/crud.scss' ]
})

export class UpdateUserComponent extends CRUDItemComponent implements OnInit {
    public updatableUser: UpdateUser;

    public teams: Team[];

    constructor(
        private readonly httpClient: HttpClientService,
        private router: Router,
        private route: ActivatedRoute
    ) {
        super();
        this.formGroup = updateUserGroup;

        this.getAllowedTeams();
    }

    ngOnInit() {
        this.route.queryParams
            .subscribe((params: Params) => {
                this.updatableUser = {
                    id: params['id'],
                    firstName: params['firstName'],
                    lastName: params['lastName'],
                    email: params['email'],
                    birthDate: params['birthDate'],
                    teamId: params['teamId']   
                };
        });
    }

    public updateTeam(): void {
        this.httpClient.put<UpdateUser, User>('/Users', this.updatableUser)
            .pipe(takeUntil(this.$unsubscribe))
            .subscribe(() => {
                this.isSaved = true;
                this.router.navigate(['/Users']);
            },
            (error: Error) => (console.warn(error))
        );
    }

    public backToMainList(): void {
        this.router.navigate(['/Users']);
    }

    private getAllowedTeams(): void {
        this.httpClient.get<Team[]>('/Teams')
            .pipe(takeUntil(this.$unsubscribe))
            .subscribe((arr: Team[]) => {
                this.teams = arr;
            },
            (error: Error) => (console.warn(error))
        );
    }
}