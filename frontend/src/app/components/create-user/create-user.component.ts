import { Component, OnDestroy } from "@angular/core";
import { Router } from "@angular/router";
import { map, takeUntil } from "rxjs/operators";
import { CRUDItemComponent } from "src/app/common/abstractions/crud-item-component";
import { createUserGroup } from "src/app/common/form-groups/userGroups";
import { Team } from "src/app/models/team/team";
import { NewUser } from "src/app/models/user/new-user";
import { User } from "src/app/models/user/user";
import { HttpClientService } from "src/app/services/http-client.service";

@Component({
    selector: 'app-create-user',
    templateUrl: 'create-user.component.html',
    styleUrls: [ './../../common/styles/crud.scss' ]
})

export class CreateUserComponent extends CRUDItemComponent {
    public creatableUser: NewUser = {
        firstName: "",
        lastName: "",
        email: "",
        birthDate: new Date()
    }

    public teams: Team[];

    constructor(
        private readonly httpClient: HttpClientService,
        private router: Router
    ) {
        super();
        this.formGroup = createUserGroup;

        this.getAllowedTeams();
    }

    public createUser(): void {
        this.httpClient.post<NewUser, User>('/Users', this.creatableUser)
            .pipe(takeUntil(this.$unsubscribe))
            .subscribe(() => {
                this.isSaved = true;
                this.router.navigate(['/Users']);
            },
            (error: Error) => (console.warn(error))
        );
    }

    public backToMainList(): void {
        this.router.navigate(['/Users']);
    }

    private getAllowedTeams(): void {
        this.httpClient.get<Team[]>('/Teams')
            .pipe(takeUntil(this.$unsubscribe))
            .subscribe((arr: Team[]) => {
                this.teams = arr;
            },
            (error: Error) => (console.warn(error))
        );
    }
}