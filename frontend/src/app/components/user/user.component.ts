import { Component } from "@angular/core";
import { User } from "src/app/models/user/user";
import { ItemComponent } from "src/app/common/abstractions/item-component";

@Component({
    selector: 'app-user',
    templateUrl: './user.component.html',
    styleUrls: [
        './user.component.scss',
        './../../common/styles/item-content.scss'
    ]
})

export class UserComponent extends ItemComponent<User> {}