import { Component } from "@angular/core";
import { Task } from "src/app/models/task/task";
import { ItemComponent } from "src/app/common/abstractions/item-component";

@Component({
    selector: 'app-task',
    templateUrl: './task.component.html',
    styleUrls: [
        './task.component.scss',
        './../../common/styles/item-content.scss'
    ]
})

export class TaskComponent extends ItemComponent<Task> {}