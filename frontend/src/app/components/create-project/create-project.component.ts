import { Component, OnDestroy } from "@angular/core";
import { Router } from "@angular/router";
import { takeUntil } from "rxjs/operators";
import { CRUDItemComponent } from "src/app/common/abstractions/crud-item-component";
import { createProjectGroup } from "src/app/common/form-groups/projectGroups";
import { NewProject } from "src/app/models/project/new-project";
import { Project } from "src/app/models/project/project";
import { Team } from "src/app/models/team/team";
import { User } from "src/app/models/user/user";
import { HttpClientService } from "src/app/services/http-client.service";

@Component({
    selector: 'app-create-project',
    templateUrl: 'create-project.component.html',
    styleUrls: [ './../../common/styles/crud.scss' ]
})

export class CreateProjectComponent extends CRUDItemComponent {
    public creatableProject: NewProject = {
        name: "",
        description: "",
        deadline: new Date(),
        authorId: 0,
        teamId: 0
    };

    public authorId: number;
    public teamId: number;

    public allowedUsers: User[];
    public teams: Team[];

    constructor(
        private readonly httpClient: HttpClientService,
        private router: Router
    ) {
        super();
        this.formGroup = createProjectGroup;

        this.getAllowedUsers();
        this.getAllowedTeams();
    }

    public createProject(): void {
        this.creatableProject.authorId = this.authorId;
        this.creatableProject.teamId = this.teamId;

        this.httpClient.post<NewProject, Project>('/Projects', this.creatableProject)
            .pipe(takeUntil(this.$unsubscribe))
            .subscribe(() => {
                this.isSaved = true;
                this.router.navigate(['/Projects']);
            },
            (error: Error) => (console.warn(error))
        );
    }

    private getAllowedUsers(): void {
        this.httpClient.get<User[]>('/Users')
            .pipe(takeUntil(this.$unsubscribe))
            .subscribe((arr: User[]) => {
                console.log(arr);
                this.allowedUsers = arr;
            },
            (error: Error) => (console.warn(error))
        );

    }

    private getAllowedTeams(): void {
        this.httpClient.get<Team[]>('/Teams')
            .pipe(takeUntil(this.$unsubscribe))
            .subscribe((arr: Team[]) => {
                this.teams = arr;
            },
            (error: Error) => (console.warn(error))
        );
    }

    public backToMainList(): void {
        this.router.navigate(['/Projects']);
    }
}