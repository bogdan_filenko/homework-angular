import { Router } from "@angular/router";
import { Component } from "@angular/core";
import { takeUntil } from "rxjs/operators";
import { Team } from "src/app/models/team/team";
import { HttpClientService } from "src/app/services/http-client.service";
import { BaseListComponent } from "src/app/common/abstractions/base-list-component";

@Component({
    selector: 'app-list-teams',
    templateUrl: './teams-list.component.html',
    styleUrls: [
        './teams-list.component.scss',
        './../../common/styles/item.scss'
    ]
})

export class TeamsListComponent extends BaseListComponent<Team> {

    constructor(
        httpClient: HttpClientService,
        router: Router
    ) {
        super(httpClient, router);

        this.httpClient.get<Team[]>(router.url)
            .pipe(takeUntil(this.$unsubscribe))
            .subscribe((values: Team[]) => {
                this.itemsList = values;
            },
            (error: Error) => {
                console.warn(error);
            });
    }
}