import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Params, Router } from "@angular/router";
import { takeUntil } from "rxjs/operators";
import { CRUDItemComponent } from "src/app/common/abstractions/crud-item-component";
import { updateTaskGroup } from "src/app/common/form-groups/taskGroups";
import { Task } from "src/app/models/task/task";
import { UpdateTask } from "src/app/models/task/update-task";
import { User } from "src/app/models/user/user";
import { HttpClientService } from "src/app/services/http-client.service";

@Component({
    selector: 'app-update-task',
    templateUrl: 'update-task.component.html',
    styleUrls: [ './../../common/styles/crud.scss' ]
})

export class UpdateTaskComponent extends CRUDItemComponent implements OnInit {
    public updatableTask: UpdateTask;

    public performers: User[];

    constructor(
        private readonly httpClient: HttpClientService,
        private router: Router,
        private route: ActivatedRoute
    ) {
        super();
        this.formGroup = updateTaskGroup;

        this.getAllowedPerformers();
    }

    ngOnInit(): void {
        this.route.queryParams
            .subscribe((params: Params) => {
                this.updatableTask = {
                    id: params['id'],
                    name: params['name'],
                    description: params['description'],
                    state: params['state'],
                    performerId: params['performerId']
                };
        });
    }

    private getAllowedPerformers(): void {
        this.httpClient.get<User[]>('/Users')
            .pipe(takeUntil(this.$unsubscribe))
            .subscribe((arr: User[]) => {
                this.performers = arr;
            },
            (error: Error) => (console.warn(error))
        );
    }

    public updateTask(): void {
        this.httpClient.put<UpdateTask, Task>('/Tasks', this.updatableTask)
            .pipe(takeUntil(this.$unsubscribe))
            .subscribe(() => {
                this.isSaved = true;
                this.router.navigate(['/Projects']);
            },
            (error: Error) => (console.warn(error))
        );
    }

    public backToMainList(): void {
        this.router.navigate(['/Projects']);
    }
}