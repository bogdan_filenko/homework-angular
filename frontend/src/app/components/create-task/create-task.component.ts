import { Component, OnDestroy, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { switchMap, takeUntil } from "rxjs/operators";
import { CRUDItemComponent } from "src/app/common/abstractions/crud-item-component";
import { createTaskGroup } from "src/app/common/form-groups/taskGroups";
import { Project } from "src/app/models/project/project";
import { NewTask } from "src/app/models/task/new-task";
import { Task } from "src/app/models/task/task";
import { User } from "src/app/models/user/user";
import { HttpClientService } from "src/app/services/http-client.service";

@Component({
    selector: 'app-create-task',
    templateUrl: 'create-task.component.html',
    styleUrls: [ './../../common/styles/crud.scss' ]
})

export class CreateTaskComponent extends CRUDItemComponent implements OnInit {
    public creatableTask: NewTask = {
        name: "",
        description: "",
        projectId: 0,
        performerId: 0
    };

    public projectId: number;
    public performerId: number;

    public performers: User[];
    public projects: Project[];

    constructor(
        private readonly httpClient: HttpClientService,
        private router: Router,
        private route: ActivatedRoute
    ) {
        super();
        this.formGroup = createTaskGroup;

        this.getAllowedPerformers();
        this.getAllowedProjects();
    }

    ngOnInit(): void {
        this.route.paramMap.pipe(
            switchMap(params => params.getAll('id'))
        )
        .subscribe((data: string) => this.projectId = +data);
    }

    public createTask(): void {
        this.creatableTask.performerId = this.performerId;
        this.creatableTask.projectId = this.projectId;

        this.httpClient.post<NewTask, Task>('/Tasks', this.creatableTask)
            .pipe(takeUntil(this.$unsubscribe))
            .subscribe((value) => {
                this.isSaved = true;
                this.router.navigate(['/Projects']);
            },
            (error: Error) => (console.warn(error))
        );
    }

    private getAllowedPerformers(): void {
        this.httpClient.get<User[]>('/Users')
            .pipe(takeUntil(this.$unsubscribe))
            .subscribe((arr: User[]) => {
                this.performers = arr;
            },
            (error: Error) => (console.warn(error))
        );
    }

    private getAllowedProjects(): void {
        this.httpClient.get<Project[]>('/Projects')
            .pipe(takeUntil(this.$unsubscribe))
            .subscribe((arr: Project[]) => {
                this.projects = arr;
            },
            (error: Error) => (console.warn(error))
        );
    }

    public backToMainList(): void {
        this.router.navigate(['/Projects']);
    }
}