import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Params, Router } from "@angular/router";
import { takeUntil } from "rxjs/operators";
import { CRUDItemComponent } from "src/app/common/abstractions/crud-item-component";
import { updateTeamGroup } from "src/app/common/form-groups/teamGroups";
import { Team } from "src/app/models/team/team";
import { UpdateTeam } from "src/app/models/team/update-team";
import { HttpClientService } from "src/app/services/http-client.service";

@Component({
    selector: 'app-update-team',
    templateUrl: 'update-team.component.html',
    styleUrls: [ './../../common/styles/crud.scss' ]
})

export class UpdateTeamComponent extends CRUDItemComponent implements OnInit {
    public updatableTeam: UpdateTeam;

    constructor(
        private readonly httpClient: HttpClientService,
        private router: Router,
        private route: ActivatedRoute
    ) {
        super();
        this.formGroup = updateTeamGroup;
    }

    ngOnInit(): void {
        this.route.queryParams
            .subscribe((params: Params) => {
                this.updatableTeam = {
                    id: params['id'],
                    name: params['name']
                };
        });
    }

    public updateTeam(): void {
        this.httpClient.put<UpdateTeam, Team>('/Teams', this.updatableTeam)
            .pipe(takeUntil(this.$unsubscribe))
            .subscribe(() => {
                this.isSaved = true;
                this.router.navigate(['/Teams']);
            },
            (error: Error) => (console.warn(error))
        );
    }

    public backToMainList(): void {
        this.router.navigate(['/Teams']);
    }
}