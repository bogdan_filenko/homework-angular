import { Component } from "@angular/core";
import { Team } from "src/app/models/team/team";
import { ItemComponent } from "src/app/common/abstractions/item-component";

@Component({
    selector: 'app-team',
    templateUrl: './team.component.html',
    styleUrls: [
        './team.component.scss',
        './../../common/styles/item-content.scss'
    ]
})

export class TeamComponent extends ItemComponent<Team> {}