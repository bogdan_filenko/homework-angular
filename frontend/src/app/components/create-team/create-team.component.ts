import { Component, OnDestroy } from "@angular/core";
import { Router } from "@angular/router";
import { map, takeUntil } from "rxjs/operators";
import { CRUDItemComponent } from "src/app/common/abstractions/crud-item-component";
import { createTeamGroup } from "src/app/common/form-groups/teamGroups";
import { NewTeam } from "src/app/models/team/new-team";
import { Team } from "src/app/models/team/team";
import { HttpClientService } from "src/app/services/http-client.service";

@Component({
    selector: 'app-create-team',
    templateUrl: 'create-team.component.html',
    styleUrls: [ './../../common/styles/crud.scss' ]
})

export class CreateTeamComponent extends CRUDItemComponent {
    public creatableTeam: NewTeam = {
        name: ""
    };

    constructor(
        private readonly httpClient: HttpClientService,
        private router: Router
    ) {
        super();
        this.formGroup = createTeamGroup;
    }

    public createTeam(): void {
        this.httpClient.post<NewTeam, Team>('/Teams', this.creatableTeam)
            .pipe(takeUntil(this.$unsubscribe))
            .subscribe(() => {
                this.isSaved = true;
                this.router.navigate(['/Teams']);
            },
            (error: Error) => (console.warn(error))
        );
    }

    public backToMainList(): void {
        this.router.navigate(['/Teams']);
    }
}