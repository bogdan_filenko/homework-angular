import { Router } from "@angular/router";
import { Component } from "@angular/core";
import { takeUntil } from "rxjs/operators";
import { Project } from "src/app/models/project/project";
import { HttpClientService } from "src/app/services/http-client.service";
import { BaseListComponent } from "src/app/common/abstractions/base-list-component";

@Component({
    selector: 'app-list-projects',
    templateUrl: './projects-list.component.html',
    styleUrls: [
        './projects-list.component.scss',
        './../../common/styles/item.scss'
    ]
})

export class ProjectsListComponent extends BaseListComponent<Project> {

    constructor(
        public httpClient: HttpClientService,
        public router: Router
    ) {
        super(httpClient, router);
        
        this.httpClient.get<Project[]>(router.url)
            .pipe(takeUntil(this.$unsubscribe))
            .subscribe((values: Project[]) => {
                this.itemsList = values;
            },
            (error: Error) => {
                console.warn(error);
            });
    }
}