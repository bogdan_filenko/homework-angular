import { Subject } from "rxjs";
import { Router } from "@angular/router";
import { takeUntil } from "rxjs/operators";
import { Component, OnDestroy } from "@angular/core";
import { HttpClientService } from "src/app/services/http-client.service";

@Component({
    selector: '',
    template: ''
})
export abstract class BaseListComponent<T extends { id: number }> implements OnDestroy {
    public itemsList: T[];
    protected $unsubscribe = new Subject();

    private routeUrl: string;

    constructor(
        protected httpClient: HttpClientService,
        protected router: Router
    ) {
        this.routeUrl = router.url;
    }

    public goToCreateMenu(): void {
        this.router.navigate([this.routeUrl + '/create']);
    }

    public deleteItem(itemId: number): void {
        this.httpClient.delete(this.routeUrl + '/' + itemId)
            .pipe(takeUntil(this.$unsubscribe))
            .subscribe(() => {
                console.log('Success');

                let taskIndex = this.itemsList.findIndex((t) => t.id === itemId);
                this.itemsList.splice(taskIndex, 1);
            },
            (error: Error) => {
                console.warn(error);
            });
    }

    ngOnDestroy(): void {
        this.$unsubscribe.next();
        this.$unsubscribe.complete();
    }
}