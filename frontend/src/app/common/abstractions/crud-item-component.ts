import { FormGroup } from "@angular/forms";
import { Observable, Subject } from "rxjs";
import { Component, OnDestroy } from "@angular/core";
import { ComponentCanDeactivate } from "src/app/guards/close-crud.guard";

@Component({
    selector: '',
    template: ''
})
export class CRUDItemComponent implements OnDestroy, ComponentCanDeactivate {
    public formGroup: FormGroup;
    protected $unsubscribe = new Subject();

    protected isSaved: boolean = false;
    
    ngOnDestroy(): void {
        this.$unsubscribe.next();
        this.$unsubscribe.complete();
    }

    public canDeactivate(): Observable<boolean> | boolean {
        return !this.isSaved
            ? confirm("You have unsaved changes. Are you sure to leave this page?")
            : true;
    }
}