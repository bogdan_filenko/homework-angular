import { FormControl, FormGroup, Validators } from "@angular/forms";

export const createUserGroup = new FormGroup({
    'firstName': new FormControl('', [
        Validators.required,
        Validators.max(30)
    ]),
    'lastName': new FormControl('', [
        Validators.required,
        Validators.max(30)
    ]),
    'email': new FormControl('', [
        Validators.required,
        Validators.email
    ]),
    'birthDate': new FormControl(new Date(), Validators.required),
    'teamId': new FormControl(0, Validators.required)
});

export const updateUserGroup = createUserGroup;