import { FormControl, FormGroup, Validators } from "@angular/forms";

export const createTaskGroup = new FormGroup({
    'name': new FormControl("", [
        Validators.required,
        Validators.maxLength(50)
    ]),
    'description': new FormControl("", [
        Validators.maxLength(140)
    ]),
    'performerId': new FormControl('', Validators.required)
});

export const updateTaskGroup = new FormGroup({
    'name': new FormControl("", [
        Validators.required,
        Validators.maxLength(50)
    ]),
    'description': new FormControl("", [
        Validators.maxLength(140)
    ]),
    'state': new FormControl(0, Validators.required),
    'performerId': new FormControl(0, Validators.required)
});