import { FormControl, FormGroup, Validators } from "@angular/forms";

export const createProjectGroup = new FormGroup({
    'name': new FormControl("", [
        Validators.required,
        Validators.maxLength(50)
    ]),
    'description': new FormControl("", [
        Validators.maxLength(140)
    ]),
    'deadline': new FormControl(new Date(), Validators.required),
    'authorId': new FormControl(0, Validators.required),
    'teamId': new FormControl(0, Validators.required)
});

export const updateProjectGroup = new FormGroup({
    'name': new FormControl("", [
        Validators.required,
        Validators.maxLength(50)
    ]),
    'description': new FormControl("", [
        Validators.maxLength(140)
    ]),
    'deadline': new FormControl(new Date(), Validators.required)
});