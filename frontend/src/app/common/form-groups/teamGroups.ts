import { FormControl, FormGroup, Validators } from "@angular/forms";

export const createTeamGroup = new FormGroup({
    'name': new FormControl("", [
        Validators.required,
        Validators.maxLength(50)
    ])
});

export const updateTeamGroup = createTeamGroup;