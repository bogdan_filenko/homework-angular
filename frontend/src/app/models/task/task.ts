import { Models } from "./task-state";

export interface Task {
    id: number;
    name?: string;
    description?: string;
    state: Models.TaskState;
    createdAt: Date;
    finishedAt?: Date;

    projectId: number;
    performerId: number;
}