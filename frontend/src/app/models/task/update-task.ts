import { Models } from "./task-state";

export interface UpdateTask {
    id: number;
    name?: string;
    description?: string;
    state: Models.TaskState;

    performerId: number;
}