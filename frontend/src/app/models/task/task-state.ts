export namespace Models {
    export enum TaskState {
        Added,
        InProcess,
        Finished,
        Refused
    }
}