export interface UpdateProject {
    id: number;
    name?: string;
    description?: string;
    deadline: Date;
}