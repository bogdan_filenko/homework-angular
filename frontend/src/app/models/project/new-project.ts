export interface NewProject {
    name?: string;
    description?: string;
    deadline: Date;

    authorId: number;
    teamId: number;
}