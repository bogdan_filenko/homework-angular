export interface Project {
    id: number;
    name?: string;
    description?: string;
    deadline: Date;
    createdAt: Date;

    authorId: number;
    teamId: number;
}