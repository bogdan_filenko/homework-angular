export interface UpdateUser {
    id: number;
    firstName?: string;
    lastName?: string;
    email?: string;
    birthDate: Date;

    teamId?: number;
}