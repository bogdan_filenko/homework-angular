export interface User {
    id: number;
    firstName?: string;
    lastName?: string;
    email?: string;
    registeredAt: Date;
    birthDate: Date;

    teamId?: number;
}