import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  host: {
    '(window:resize)': 'onResize($event)'
  }
})
export class AppComponent {
  public isMenuVisible: boolean = false;
  public screenWidth: number = window.screen.width;

  public onResize(event: Event) {
    this.screenWidth = (event.target as Window).screen.width;
  }

  public showMenu(): void {
    this.isMenuVisible = !this.isMenuVisible;
  }
}
