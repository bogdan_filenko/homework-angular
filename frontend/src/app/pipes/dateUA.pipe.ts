import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
    name: 'dateUA'
})

export class DateUAPipe implements PipeTransform {
    transform(date: Date | undefined): string {
        return new Date(date as Date).toLocaleDateString('uk-UA', this.makeOptions());
    }

    private makeOptions(): Intl.DateTimeFormatOptions {
        return {
            year: 'numeric',
            month: 'long',
            day: 'numeric'
        };
    }
}